'''
Created on Feb 5, 2018

@author: YASWANTH
'''


import time
from watchdog.observers import Observer
from watchdog.events import FileSystemEventHandler

from sample import EodLoanJob 

def getArgs(argv):
    opts = {}  
    while argv: 
        if argv[0][0] == '-':  # Found a "-name value" pair.
            opts[argv[0]] = argv[1]  # Add key and value to the dictionary.
    return opts

class EodLoanFileWatcher:    
    from sys import argv
    myargs = getArgs(argv)
    if '-i' in myargs:  
        DIRECTORY_TO_WATCH = myargs['-i']  # input directory monitor
    
    def __init__(self):
        self.observer = Observer()

    def run(self):
        event_handler = Handler()
        self.observer.schedule(event_handler, self.DIRECTORY_TO_WATCH, recursive=True)
        self.observer.start()
        try:
            while True:
                time.sleep(10)
        except:
            self.observer.stop()
            print ("Error")

        self.observer.join()


class Handler(FileSystemEventHandler):

    @staticmethod
    def on_any_event(event):
        if event.is_directory:
            return None

        print ("Received created event - %s." % event.src_path)
        import os
        for file in os.listdir(event.src_path):
            if file.endswith(".csv"):
                print(os.path.join(event.src_path, file))
                eodLoanJob = EodLoanJob()
                eodLoanJob.processFile(os.path.join(event.src_path, file))
     
if __name__ == '__main__':
    w = EodLoanFileWatcher()
    w.run()