'''
Created on Feb 5, 2018

@author: YASWANTH
'''

from multiprocessing import Pool

import threading
import Queue

from sample import  LoanInfo


class EodLoanJob():
    
   
    def doWork(self,in_queue, out_queue):
        while True:
            item = in_queue.get()
            # process
            result = self.processLoan(item)
            out_queue.put(result)
            in_queue.task_done()
        
            
    def processLoan(self,line):
        loanData = line.split(",")
        loanInfoObj = LoanInfo()  
        loanInfoObj.loanNumber = loanData[0]  # loan number  
        loanInfoObj.loanType = loanData[1]  # type of the loan
        loanInfoObj.borrowerName = loanData[3] # borrower last and first name
        loanInfoObj.action = loanData[4] # new or update loan action
        loanInfoObj.RequestAmount = loanData[5] # requested loan amount
        loanInfoObj.approvedAmount = loanData[6] # approved loan amount
        loanInfoObj.interestRate = loanData[8] # interest rate of the loan
        loanInfoObj.paymentType = loanData[9] # payment type monthly or quarterly
        loanInfoObj.useOfProceedAmount= loanData[15] # loan amount used for
        loanInfoObj.disburseAmount= loanData[16] #disbursed loan amount that is released
        validateStatus = loanInfoObj.validateLoan() 
        if validateStatus:
            loanStatus = loanInfoObj.save();
            
        else:
            print("validation failed")
            loanStatus= loanInfoObj.loanNumber  + ":VALIDATION_FAILED"
            
        return loanStatus   
        pass
    
  
    
    
    def processFile(self,inFileName):
        print("processing input fileName %s" %inFileName)
        work = Queue.Queue()
        results = Queue.Queue() 
                   
        # start for workers
        threads = []
        for j in xrange(5): 
            t = threading.Thread(target=self.doWork, args=(work, results))
            threads.append(t)
            t.daemon = True
            t.start()
             
        with open(inFileName) as f:   
            line = f.readline()
            ## If the file is not empty keep reading line one at a time till the file is empty
            while line:
                print(line)
                line = f.readline()
                work.put(line)
        f.close()
        # waiting all work is completed
        work.join()
        print("processed input file successfully %s" %inFileName)
    
    
        




    
